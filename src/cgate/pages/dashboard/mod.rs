use errors::*;
use rocket_contrib::Template;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form},
    response::{self, Redirect, Responder},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    posts::PostsDb,
    services::ServicesDb,
    users::{
        User,
        UsersDb,
        types::{Username, FirstName, LastName, Phone, Email, Password},
    },
};

pub mod news;
pub mod news_add;
pub mod news_delete;
pub mod news_edit;
pub mod users;
pub mod contact_us_messages;

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    published_news: u32,
    draft_news: u32,
    users: u32,
    clubs: u32,
    available_courses: u32,
    courses: u32,
    answered_contact_us_messages: u32,
    archived_contact_us_messages: u32,
    waiting_contact_us_messages: u32,
}

pub struct DashboardPage {
    conn: DbConn,
    admin: User,
}

impl<'r> DashboardPage {
    fn new(conn: DbConn, admin: User) -> Self {
        Self {
            conn,
            admin,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = super::get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            published_news: 0,
            draft_news: 0,
            users: 0,
            clubs: 0,
            available_courses: 0,
            courses: 0,
            answered_contact_us_messages: 0,
            archived_contact_us_messages: 0,
            waiting_contact_us_messages: 0,
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();

        context.published_news = self.get_posts_number()
            .map_err(|_| Status::InternalServerError)?;

        context.draft_news = self.get_draft_posts_number()
            .map_err(|_| Status::InternalServerError)?;

        context.users = self.get_users_number()
            .map_err(|_| Status::InternalServerError)?;

        context.clubs = self.get_clubs_number()
            .map_err(|_| Status::InternalServerError)?;

        context.courses = self.get_courses_number()
            .map_err(|_| Status::InternalServerError)?;

        context.answered_contact_us_messages = self.get_answered_contact_us_messages_number()
            .map_err(|_| Status::InternalServerError)?;

        context.archived_contact_us_messages = self.get_archived_contact_us_messages_number()
            .map_err(|_| Status::InternalServerError)?;

        context.waiting_contact_us_messages = self.get_waiting_contact_us_messages_number()
            .map_err(|_| Status::InternalServerError)?;

        let template = Template::render("dashboard/index", context);
        template.respond_to(req)
    }
}

impl<'r> Responder<'r> for DashboardPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        self.index_handler(req)
    }
}

impl UsersDb for DashboardPage { }
impl ServicesDb for DashboardPage { }
impl AboutUsDb for DashboardPage { }
impl ClubsDb for DashboardPage { }
impl ContactUsMessagesDb for DashboardPage { }
impl CoursesDb for DashboardPage { }
impl PostsDb for DashboardPage { }

impl DatabaseConnection for DashboardPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

pub fn dashboard_routes() -> Vec<Route> {
    let mut routes = routes![dashboard];
    routes.append(&mut news::d_news_routes());
    routes.append(&mut news_add::d_news_add_routes());
    routes.append(&mut news_delete::d_news_delete_routes());
    routes.append(&mut news_edit::d_news_edit_routes());
    routes.append(&mut users::d_users_routes());
    routes.append(&mut contact_us_messages::d_contact_us_messages_routes());
    routes
}

#[get("/dashboard")]
pub fn dashboard(conn: DbConn, admin: User) -> DashboardPage {
    DashboardPage::new(conn, admin)
}

use errors::*;
use rocket_contrib::Template;
use pages::Pagination;
use pages::get_site_links;
use rocket::{
    Route,
    http::Status,
    request::{Request, Form},
    response::{self, Redirect, Responder},
};
use contexts::Link;
use database::{
    DbConn,
    DatabaseConnection,
    about_us::AboutUsDb,
    clubs::ClubsDb,
    contact_us_messages::ContactUsMessagesDb,
    courses::CoursesDb,
    posts::{PostContext, PostsDb},
    services::ServicesDb,
    users::{
        User,
        UserContext,
        UsersDb,
        types::{Username, FirstName, LastName, Phone, Email, Password},
    },
};

#[derive(Debug, Serialize)]
pub struct PageContext {
    phone_buttons: Vec<Link>,
    phone_links: Vec<Link>,
    links: Vec<Link>,
    news: Vec<PostContext>,
    pagination: Vec<Link>,
    user: UserContext,
}

pub struct DNewsPage {
    conn: DbConn,
    current_page: u32,
    admin: User,
}

impl<'r> DNewsPage {
    fn new(conn: DbConn, current_page: u32, admin: User) -> Self {
        Self {
            conn,
            current_page,
            admin,
        }
    }

    fn get_context(&mut self) -> PageContext {
        // get links for the sidebar view
        let links = get_site_links();
        let phone_buttons: Vec<Link> = links.iter()
            .take(2)
            .map(|link| link.clone())
            .collect();
        let phone_links: Vec<Link> = links.iter()
            .skip(2)
            .map(|link| link.clone())
            .collect();

        PageContext {
            phone_buttons: phone_buttons,
            phone_links: phone_links,
            links: links,
            news: vec![],
            pagination: vec![],
            user: self.admin.clone().into(),
        }
    }

    fn index_handler(&mut self, req: &Request) -> response::Result<'r> {
        let mut context = self.get_context();

        context.news = self.get_posts_context()
            .map_err(|_| Status::InternalServerError)?;

        // convert pagination from Vec<u32> into Vec<Link>
        let mut pagination = self.pagination_links("/dashboard/news/")
            .map_err(|_| Status::InternalServerError)?;
        // revers the numbers so it feels like arabic language RTL
        pagination.reverse();
        context.pagination = pagination;

        let template = Template::render("dashboard/news", context);
        template.respond_to(req)
    }
}

impl<'r> Responder<'r> for DNewsPage {
    fn respond_to(mut self, req: &Request) -> response::Result<'r> {
        if !self.admin.is_admin() {
            return Err(Status::NotFound);
        }
        self.index_handler(req)
    }
}

impl UsersDb for DNewsPage { }
impl PostsDb for DNewsPage { }

impl DatabaseConnection for DNewsPage {
    fn db_connection(&self) -> &DbConn {
        &self.conn
    }
}

impl Pagination for DNewsPage {
    fn items_per_page(&self) -> u32 {
        5
    }

    fn pagination_level(&self) -> u32 {
        6
    }

    fn all_items(&self) -> Result<u32> {
        self.get_posts_number()
    }

    fn page(&self) -> u32 {
        self.current_page
    }

    fn set_page(&mut self, page: u32) {
        self.current_page = page;
    }
}

pub fn d_news_routes() -> Vec<Route> {
    routes![d_news]
}

#[get("/dashboard/news/<page>")]
pub fn d_news(conn: DbConn, admin: User, page: u32) -> DNewsPage {
    DNewsPage::new(conn, page, admin)
}

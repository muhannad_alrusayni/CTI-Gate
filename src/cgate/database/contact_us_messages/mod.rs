use diesel::prelude::*;
use errors::*;
use self::types::*;
use checked_types::*;
use pages::contact_us::MessageForm;
use pages::Pagination;
use std::{
    ops::Deref,
    convert::TryFrom,
};
use database::{
    DatabaseConnection,
    schema::{contact_us_messages as contact_us_messages_table}
};

pub mod types;

#[derive(Queryable, Insertable, Debug, Clone, Serialize, Deserialize)]
#[table_name = "contact_us_messages_table"]
pub struct ContactUsMessage {
    pub id: Id,
    pub sent_at: DateTime,
    pub nickname: Username,
    pub email: Email,
    pub phone: Option<Phone>,
    pub status: Status,
    pub category: Category,
    pub subject: Subject,
    pub content: Content,
}

impl ContactUsMessage {
    pub fn new(
        id: Id,
        sent_at: DateTime,
        nickname: Username,
        email: Email,
        phone: Option<Phone>,
        status: Status,
        category: Category,
        subject: Subject,
        content: Content
    ) -> Self {
        Self {
            id: id,
            sent_at: sent_at,
            nickname: nickname,
            email: email,
            phone: phone,
            status: status,
            category: category,
            subject: subject,
            content: content,
        }
    }

    pub fn new_from_message_form(
        id: Id,
        sent_at: DateTime,
        status: Status,
        message: &MessageForm,
    ) -> Result<Self> {
        fn ok_or_incomplete_message_form<T: Clone>(value: &Result<T>) -> Result<T> {
            match value {
                Ok(ref value) => Ok(value.clone()),
                Err(ref err) => Err(ErrorKind::IncompleteMessageForm(format!("{}", err)))?,
            }
        };

        let nickname = ok_or_incomplete_message_form(&message.nickname)?;
        let email = ok_or_incomplete_message_form(&message.email)?;
        let phone = match message.phone {
            Some(ref value) => Some(ok_or_incomplete_message_form(value)?),
            None => None,
        };
        let category = ok_or_incomplete_message_form(&message.category)?;
        let subject = ok_or_incomplete_message_form(&message.subject)?;
        let content = ok_or_incomplete_message_form(&message.content)?;

        let contact_us_message = Self::new(
            id,
            sent_at,
            nickname,
            email,
            phone,
            status,
            category,
            subject,
            content,
        );

        Ok(contact_us_message)
    }
}

// TODO: impl From<ContactUsMessage> ..
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ContactUsMessageContext {
    pub sent_at: DateTime,
    pub nickname: Username,
    pub email: Email,
    pub phone: Option<Phone>,
    pub subject: Subject,
    pub content: Content,
}

pub trait ContactUsMessagesDb: DatabaseConnection {
    ///
    fn get_messages_for_current_page(&self) -> Result<Vec<ContactUsMessage>>
    where
        Self: Pagination,
    {
        use database::schema::contact_us_messages::dsl::*;;

        let page = self.page();
        self.is_vaild_page_number(page)?;

        let items_per_page = self.items_per_page();
        let conn = self.db_connection();
        let offset = items_per_page * (page as i32 - 1).abs() as u32;

        let users_context = contact_us_messages.order(sent_at.desc())
            .limit(items_per_page as i64)
            .offset(offset as i64)
            .load::<ContactUsMessage>(conn.deref())
            .chain_err(|| "Couldn't get contact us messages from db")?;

        Ok(users_context)
    }

    ///
    fn get_all_contact_us_messages_numbers(&self) -> Result<u32> {
        use database::schema::contact_us_messages::dsl::*;

        let messages_number = contact_us_messages.count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get contact us messages number from db")?
            .pop();

        match messages_number {
            None => Ok(0),
            Some(number) => Ok(number as u32),
        }
    }

    ///
    fn get_contact_us_messages_number_by(&self, stat: Status) -> Result<u32> {
        use database::schema::contact_us_messages::dsl::*;

        let messages_number = contact_us_messages
            .filter(status.eq(stat))
            .count()
            .load::<i64>(self.db_connection().deref())
            .chain_err(|| "Couldn't get contact us messages number from dbr")?
            .pop();

        Ok(messages_number.unwrap_or(0) as u32)
    }

    ///
    fn get_answered_contact_us_messages_number(&self) -> Result<u32> {
        self.get_contact_us_messages_number_by(Status::Replayed)
    }

    ///
    fn get_waiting_contact_us_messages_number(&self) -> Result<u32> {
        self.get_contact_us_messages_number_by(Status::Waiting)
    }

    ///
    fn get_archived_contact_us_messages_number(&self) -> Result<u32> {
        self.get_contact_us_messages_number_by(Status::Archived)
    }

    // get max id in contact_us_users table
    fn get_max_id(&self) -> Result<Option<Id>> {
        use database::schema::contact_us_messages::dsl::*;
        use diesel::dsl::max;


        let max_id = contact_us_messages.order(id)
            .select(max(id))
            .first::<Option<Id>>(self.db_connection().deref())?;

        Ok(max_id)
    }

    // add message to the database
    fn add_message(&self, message: &MessageForm) -> Result<()> {
        use diesel::{self, select};

        let id = match self.get_max_id()? {
            Some(value) => value.checked_add(1)?,
            None => Id::try_from(0)?,
        };
        let sent_at = select(diesel::dsl::now)
            .first::<DateTime>(self.db_connection().deref())?;
        let status = Default::default();

        let message =
            ContactUsMessage::new_from_message_form(id, sent_at, status, message)?;

        // insert the data after making sure that all data we got are valid
        {
            use diesel::insert_into;
            use database::schema::contact_us_messages::dsl::*;
            insert_into(contact_us_messages)
                .values(message)
                .execute(self.db_connection().deref())
                .chain_err(|| "Couldn't add ContactUsMessage")?;
        }

        Ok(())
    }

    ///
    fn delete_message(&self, mid: &Id) -> Result<()> {
        use diesel::delete;
        use database::schema::contact_us_messages::dsl::*;

        delete(contact_us_messages.filter(id.eq(mid)))
            .execute(self.db_connection().deref())
            .chain_err(|| "Couldn't delete message from db")?;

        Ok(())
    }
}

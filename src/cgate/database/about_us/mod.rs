use self::types::*;
use errors::*;
use diesel::prelude::*;
use database::DatabaseConnection;
use std::ops::Deref;

pub mod types;

#[derive(Queryable, Debug, Clone, Serialize, Deserialize)]
pub struct AboutUs {
    id: Id,
    title: Title,
    info: Info,
}

pub type AboutUsContext = AboutUs;

pub trait AboutUsDb: DatabaseConnection {
    // get all infos in about_us table
    fn get_about_us_context(&self) -> Result<Vec<AboutUsContext>> {
        use database::schema::about_us::dsl::*;

        let infos = about_us.load::<AboutUs>(self.db_connection().deref())
            .chain_err(|| "Couldn't load about us from db")?
            .into_iter()
            .collect();

        Ok(infos)
    }
}

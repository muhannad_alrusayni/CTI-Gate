table! {
    about_us (id) {
        id -> Integer,
        title -> Text,
        info -> Text,
    }
}

table! {
    clubs (id) {
        id -> Integer,
        clubname -> Text,
        page_id -> Text,
        page_content -> Text,
        description -> Text,
        started_at -> Timestamp,
        scores -> Integer,
        manager -> Integer,
    }
}

table! {
    clubs_members (club_id, user_id) {
        club_id -> Integer,
        user_id -> Integer,
        registered_at -> Timestamp,
        job -> Integer,
    }
}

table! {
    contact_us_messages (id) {
        id -> Integer,
        sent_at -> Timestamp,
        nickname -> Text,
        email -> Text,
        phone -> Nullable<Text>,
        status -> Integer,
        category -> Integer,
        subject -> Text,
        content -> Text,
    }
}

table! {
    courses (id) {
        id -> Integer,
        title -> Text,
        description -> Text,
        start_at -> Timestamp,
        url -> Text,
        teacher -> Nullable<Integer>,
    }
}

table! {
    courses_tags (id) {
        id -> Integer,
        tag -> Text,
        course_id -> Integer,
    }
}

table! {
    posts (id) {
        id -> Integer,
        title -> Text,
        summary -> Text,
        page_id -> Text,
        page_content -> Text,
        published_at -> Timestamp,
        draft -> Bool,
        author -> Integer,
        club -> Nullable<Integer>,
    }
}

table! {
    posts_tags (id) {
        id -> Integer,
        tag -> Text,
        post_id -> Integer,
    }
}

table! {
    services (id) {
        id -> Integer,
        title -> Text,
        page_id -> Text,
        summary -> Text,
        page_content -> Text,
        started_at -> Nullable<Timestamp>,
    }
}

table! {
    users (id) {
        id -> Integer,
        username -> Text,
        page_id -> Text,
        first_name -> Text,
        last_name -> Text,
        permission -> Integer,
        status -> Integer,
        phone -> Nullable<Text>,
        email -> Text,
        password -> Text,
        slat -> Text,
        registered_at -> Timestamp,
    }
}

joinable!(clubs -> users (manager));
joinable!(clubs_members -> clubs (club_id));
joinable!(clubs_members -> users (user_id));
joinable!(courses -> users (teacher));
joinable!(courses_tags -> courses (course_id));
joinable!(posts -> clubs (club));
joinable!(posts -> users (author));
joinable!(posts_tags -> posts (post_id));

allow_tables_to_appear_in_same_query!(
    about_us,
    clubs,
    clubs_members,
    contact_us_messages,
    courses,
    courses_tags,
    posts,
    posts_tags,
    services,
    users,
);

INSERT INTO
users (id, username, page_id, first_name, last_name, permission, status, phone, email, password, slat)
VALUES (0, 'مدير المركز', 'admin', 'عبدالملك', 'العرفج', 0, 1, '0504567890', 'a@gmai.com',
       'c7514961abff91a2f73c5f4bf178392fd6b2d3926591477f21371ee22e19dbcb',
       'WS2zkOa5tswm4fXgdtOc0ryDqIJEDG73XaxnJPMLnrjDYV33gIFx1xP2PmvaM2AD'),
       (1, 'Rust Developer', 'rust_dev', 'مهند', 'الرسيني', 10, 1, '0535437704', 'muhannad.alrusayni@gmail.com',
       'd23b8a04e54631d0480e1c2fcb17cf2d8a470d74978ce2bb0b677fe13f3521d3',
       '1vXkfnFOxZDPmEz1g2Nc6lTWM1OMueEwKyXMSp1Tq6M5J09TqDQwgguwAPF1dp7B'),
       (2, 'محمد', 'mohmmad', 'محمد' , 'الرسيني', 100, 1, '0544567890', 'mohmmad@i.me',
       'd23b8a04e54631d0480e1c2fcb17cf2d8a470d74978ce2bb0b677fe13f3521d3',
       '1vXkfnFOxZDPmEz1g2Nc6lTWM1OMueEwKyXMSp1Tq6M5J09TqDQwgguwAPF1dp7B'),
       (3, 'xXAliXx', 'ali', 'علي', 'العرفج', 100, 0, '0535437733', 'ali@g.me',
       'd23b8a04e54631d0480e1c2fcb17cf2d8a470d74978ce2bb0b677fe13f3521d3',
       '1vXkfnFOxZDPmEz1g2Nc6lTWM1OMueEwKyXMSp1Tq6M5J09TqDQwgguwAPF1dp7B');

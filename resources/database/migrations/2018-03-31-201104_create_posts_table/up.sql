CREATE TABLE posts(
    id              INT             NOT NULL PRIMARY KEY,
    title           VARCHAR(50)     NOT NULL UNIQUE,
    summary         VARCHAR(150)    NOT NULL UNIQUE,
    page_id         VARCHAR(50)     NOT NULL UNIQUE,
    page_content    TEXT            NOT NULL,
    published_at    DATETIME        NOT NULL DEFAULT (datetime('now')),
    draft           BOOLEAN         NOT NULL DEFAULT 0 CHECK (draft IN (1, 0)),
    author          INT             NOT NULL REFERENCES users(id),
    club            INT             REFERENCES clubs(id)
)
